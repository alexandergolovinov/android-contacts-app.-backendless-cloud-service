package com.alexang.backendlessservice.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.alexang.backendlessservice.R;
import com.alexang.backendlessservice.entity.Contact;

import java.util.List;

public class ContactAdapter extends ArrayAdapter<Contact> {

    private Context context;
    private List<Contact> mContactList;

    public ContactAdapter(Context context, List<Contact> list) {
        super(context, R.layout.layout_contact_single_item, list);
        this.context = context;
        this.mContactList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.layout_contact_single_item, parent, false);

        final TextView textChar = convertView.findViewById(R.id.text_character);
        final TextView textName = convertView.findViewById(R.id.text_name);
        final TextView textEmail = convertView.findViewById(R.id.text_email);

        textChar.setText(mContactList.get(position).getName().toUpperCase().charAt(0) + "");
        textName.setText(mContactList.get(position).getName());
        textEmail.setText(mContactList.get(position).getEmail());

        return convertView;
    }
}
