package com.alexang.backendlessservice;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alexang.backendlessservice.application.ApplicationClass;
import com.alexang.backendlessservice.entity.Contact;
import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

public class InfoActivity extends Activity {

    private View mProgressView;
    private View mLoginFormView;
    private TextView mTextLoad;

    private TextView mTextChar;
    private TextView mTextName;

    private EditText mEditName;
    private EditText mEditMail;
    private EditText mEditPhone;

    private Button mButtonSubmit;

    private boolean isEditing = false;
    public static int index = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        mProgressView = findViewById(R.id.login_progress);
        mLoginFormView = findViewById(R.id.login_form);
        mTextLoad = findViewById(R.id.text_load);

        mTextChar = findViewById(R.id.text_info_char);
        mTextName = findViewById(R.id.text_info_name);

        mEditName = findViewById(R.id.edit_info_name);
        mEditMail = findViewById(R.id.edit_info_email);
        mEditPhone = findViewById(R.id.edit_info_phone);

        mButtonSubmit = findViewById(R.id.btn_info_submit);

        mEditName.setVisibility(View.GONE);
        mEditMail.setVisibility(View.GONE);
        mEditPhone.setVisibility(View.GONE);
        mButtonSubmit.setVisibility(View.GONE);
        index = getIntent().getIntExtra("index", 0);

        final String contactName = ApplicationClass.contacts.get(index).getName();
        final String contactMail = ApplicationClass.contacts.get(index).getEmail();
        final String contactPhone = ApplicationClass.contacts.get(index).getNumber();

        mTextChar.setText(contactName.toUpperCase().charAt(0) + "");
        mTextName.setText(contactName);

        mEditName.setText(contactName);
        mEditMail.setText(contactMail);
        mEditPhone.setText(contactPhone);


        mButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String nameEdited = mEditName.getText().toString().trim();
                final String mailEdited = mEditMail.getText().toString().trim();
                final String phoneEdited = mEditPhone.getText().toString().trim();
                if (TextUtils.isEmpty(nameEdited) || TextUtils.isEmpty(mailEdited) || TextUtils.isEmpty(phoneEdited)) {
                    Toast.makeText(InfoActivity.this, "Please fill all fields", Toast.LENGTH_SHORT);
                } else {
                    ApplicationClass.contacts.get(index).setName(nameEdited);
                    ApplicationClass.contacts.get(index).setEmail(mailEdited);
                    ApplicationClass.contacts.get(index).setNumber(phoneEdited);
                    showProgress(true);
                    mTextLoad.setText("Updating contact... please wait...");

                    Backendless.Persistence.save(ApplicationClass.contacts.get(index), new AsyncCallback<Contact>() {
                        @Override
                        public void handleResponse(Contact response) {
                            mTextChar.setText(ApplicationClass.contacts.get(index).getName().toUpperCase().charAt(0) + "");
                            mTextName.setText(ApplicationClass.contacts.get(index).getName());
                            showProgress(false);
                            Toast.makeText(InfoActivity.this, "Contact updated successfully", Toast.LENGTH_LONG);
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Toast.makeText(InfoActivity.this, fault.getMessage(), Toast.LENGTH_SHORT);
                            showProgress(false);
                        }
                    });
                }
            }
        });
    }

    public void onCall(View view) {
        String uri = "tel:" + ApplicationClass.contacts.get(index).getNumber();
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

    public void onMail(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        intent.putExtra(Intent.EXTRA_EMAIL, ApplicationClass.contacts.get(index).getEmail());
        startActivity(Intent.createChooser(intent,
                "Send mail to " + ApplicationClass.contacts.get(index).getName()));
    }

    public void onEdit(View view) {
        isEditing = !isEditing; //Flag for editing. If true then show visibility. Toggle effect

        if (isEditing) {
            mEditName.setVisibility(View.VISIBLE);
            mEditMail.setVisibility(View.VISIBLE);
            mEditPhone.setVisibility(View.VISIBLE);
            mButtonSubmit.setVisibility(View.VISIBLE);
        } else {
            mEditName.setVisibility(View.GONE);
            mEditMail.setVisibility(View.GONE);
            mEditPhone.setVisibility(View.GONE);
            mButtonSubmit.setVisibility(View.GONE);
        }
    }

    public void onDelete(View view) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(InfoActivity.this);
        dialog.setMessage("Are you sure you want to delete the contact " +
                ApplicationClass.contacts.get(index).getName() + " from the contact list?");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showProgress(true);
                mTextLoad.setText("Deleting the contact");
                Backendless.Persistence.of(Contact.class).remove(ApplicationClass.contacts.get(index), new AsyncCallback<Long>() {
                    @Override
                    public void handleResponse(Long response) {
                        ApplicationClass.contacts.remove(index);
                        Toast.makeText(InfoActivity.this, "Successfully removed", Toast.LENGTH_SHORT);
                        setResult(RESULT_OK);
                        InfoActivity.this.finish();
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(InfoActivity.this, fault.getMessage(), Toast.LENGTH_SHORT);
                    }
                });
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
            mTextLoad.setVisibility(show ? View.VISIBLE : View.GONE);
            mTextLoad.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mTextLoad.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mTextLoad.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
