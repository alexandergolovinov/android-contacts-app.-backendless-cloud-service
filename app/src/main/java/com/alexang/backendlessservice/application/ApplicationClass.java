package com.alexang.backendlessservice.application;

import android.app.Application;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;

import java.util.List;

import com.alexang.backendlessservice.entity.Contact;

public class ApplicationClass extends Application {

    public static final String APP_ID = "CA633EA1-0991-9632-FF9D-1DB0E4366200";
    public static final String ANDROID_API_KEY = "22BBE90B-36BB-48FE-FF0F-A7956EDB2400";
    public static final String SERVER_URL = "https://api.backendless.com";

    public static BackendlessUser user;
    public static List<Contact> contacts;

    @Override
    public void onCreate() {
        super.onCreate();
        Backendless.setUrl(ApplicationClass.SERVER_URL);
        Backendless.initApp(getApplicationContext(), APP_ID
                , ANDROID_API_KEY);
    }
}
